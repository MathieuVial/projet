from setuptools import setup

setup (
    name = "SimpleCalculator" ,
    version = "0.0.1",
    autor ="Mathieu VIAL",
    packages = ['calculator_pkg'],
    description = "SimpleCalculator is a simple programm to calculate",
    license="GNU GPLv3",
    pyton_requires = ">=2,4",
)